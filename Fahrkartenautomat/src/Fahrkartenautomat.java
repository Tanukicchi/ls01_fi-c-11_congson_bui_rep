import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       byte numberoftickets;

       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       System.out.print("Anzahl der Tickets (max. 127): ");
       numberoftickets = tastatur.nextByte();
       zuZahlenderBetrag = zuZahlenderBetrag * numberoftickets;

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
    	   
       {
    	 System.out.println("Noch zu zahlen: " + String.format("%.2f", numberoftickets * (zuZahlenderBetrag - eingezahlterGesamtbetrag)));
    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(r�ckgabebetrag > 0.0)
       {
    	   System.out.println("Der R�ckgabebetrag in H�he von " + String.format("%.2f", r�ckgabebetrag) + " EURO");
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

    	   while(r�ckgabebetrag >= 200.00)// 200 EURO-Schein
           {
        	  System.out.println("200 EURO");
 	          r�ckgabebetrag -= 200.00;
           }
    	   while(r�ckgabebetrag >= 100.00)// 100 EURO-Schein
           {
        	  System.out.println("100 EURO");
 	          r�ckgabebetrag -= 100.00;
           }
    	   while(r�ckgabebetrag >= 50.00)// 50 EURO-Schein
           {
        	  System.out.println("50 EURO");
 	          r�ckgabebetrag -= 50.00;
           }
    	   while(r�ckgabebetrag >= 20.00)// 20 EURO-Schein
           {
        	  System.out.println("20 EURO");
 	          r�ckgabebetrag -= 20.00;
           }
    	   while(r�ckgabebetrag >= 10.00)// 10 EURO-Schein
           {
        	  System.out.println("10 EURO");
 	          r�ckgabebetrag -= 10.00;
           }
    	   while(r�ckgabebetrag >= 5.0)// 5 EURO-Schein
           {
        	  System.out.println("5 EURO");
 	          r�ckgabebetrag -= 5.00;
           }
    	   while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
           while(r�ckgabebetrag >= 0.02)// 2 CENT-M�nzen
           {
        	  System.out.println("2 CENT");
 	          r�ckgabebetrag -= 0.02;
           }
           while(r�ckgabebetrag >= 0.01)// 1 CENT-M�nzen
           {
        	  System.out.println("1 CENT");
 	          r�ckgabebetrag -= 0.01;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}


/* double zuZahlenderBetrag; double eingezahlterGesamtbetrag; double eingeworfeneM�nze; double r�ckgabebetrag; byte numberoftickets
Punkt- und Strichrechnung wird durchgef�hrt
*/ 

