/**
  *   Aufgabe:  Recherchieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Cong Son Bui >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnensystem                    
    int anzahlPlaneten = 8;
    
    // Anzahl der Sterne in unserer Milchstra�e
    long anzahlSterne = 250_000_000_000L;
    
    // Wie viele Einwohner hat Berlin?
    long bewohnerBerlin = 3_000_000_000L;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 8124;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 190000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroesstesLand = 117_000_000;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    float flaecheKleinstesLand = 0.49f;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    System.out.println("Einwohnerzahl Berlin: " + bewohnerBerlin);
    System.out.println("Mein Alter in Tagen: " + alterTage);
    System.out.println("schwerstes Tier der Welt wiegt: " + gewichtKilogramm);
    System.out.println("groesstes Land der Erde in km�: " + flaecheGroesstesLand);
    System.out.println("kleinstes Land der Erde in km�: " + flaecheKleinstesLand);
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}