
public class A3 {

	public static void main(String[]args) {
		
		String f = "Fahrenheit";
		String c = "Celsius";
		
		//Table 
		System.out.printf("%-12s|", f);
		System.out.printf("%10s\n", c);
		System.out.print("-----------------------\n");
		
		//Celcius Values
		int g = -20;
		int h = -10;
		int i = 0;
		int j = 20;
		int k = 30;
		
		//Fahrenheit Values
		double l = 28.8889;
		double m = 23.3333;
		double n = 17.7778;
		double o = -6.6667;
		double p = -1.1111;
		
		//static table
		System.out.printf("%-12.2f|",l);
		System.out.printf("%10s\n", g);
		System.out.printf("%-12.2f|",m);
		System.out.printf("%10s\n", h);
		System.out.printf("%-12.2f|",n);
		System.out.printf("%10s\n", i);
		System.out.printf("%-12.2f|",o);
		System.out.printf("%10s\n", j);
		System.out.printf("%-12.2f|",p);
		System.out.printf("%10s\n", k);
		
	}
}
