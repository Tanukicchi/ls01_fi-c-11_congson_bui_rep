import java.util.Scanner;
public class Mittelwert {
	
	public static void main(String[] args) {
		
		double num1, num2, m;
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		System.out.println("Bitte geben Sie die erste Zahl ein: ");
		
		num1 = myScanner.nextDouble();
		
		System.out.println("Bitte geben Sie die zweite Zahl ein: ");
		num2 = myScanner.nextDouble();
		
		m = (num1 + num2) / 2.0;
		
		System.out.println("Mittewert ist: " + m);
		
		myScanner.close();
		
	}

}
