import java.util.Scanner;
public class Mittelwert_mit_Methode {
	
	public static void main(String[] args) {
	double zahl1;
	double zahl2;
	double mittelwert;
	Scanner myScanner = new Scanner(System.in);
	System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
	zahl1 = eingabe(myScanner, "Bitte geben Sie die erste Zahl ein: ");
	zahl2 = eingabe(myScanner, "Bitte geben Sie die zweite Zahl ein: ");
	mittelwert = mittelwertBerechnung(zahl1, zahl2);
	ausgabe(mittelwert);
	myScanner.close();
	
	}
	
	public static double eingabe(Scanner myScanner, String text) {
		
		System.out.println(text);
		double eingabe = myScanner.nextDouble();
		return eingabe;
			
	}
	
	public static double mittelwertBerechnung(double zahl1, double zahl2) {
		
		double m = (zahl1 + zahl2) / 2.0;
		return m;
	}
	
	public static void ausgabe(double mittelwert) {
		
		System.out.println("Der Mittelwert ist: " + mittelwert);
	}
	
}